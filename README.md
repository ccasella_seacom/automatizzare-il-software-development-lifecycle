# Automatizzare il Software Development LifeCycle

Questo repository è un progetto demo utilizzato durante conferenze e training per mostrare come automattizare il Software Development Lifecycle con GitLab ed altri strumenti.

Il software utilizza la versione Community di GitLab con l'aggiunta di alcuni strumenti secondari.

E' possibile scaricare le slide associate a questo progetto da [questo link](https://www.seacom.it/automatizzare-il-software-development-lyfecycle/).

## Renovate Bot

Il progetto fa utilizzo anche di Renovate Bot, il relativo progetto è disponibile [qui](https://gitlab.com/ccasella_seacom/renovate-bot).

## GitLab Trial

Se vuoi scoprire di più su GitLab Enterprise, di seguito due link per ricevere una licenza di prova gratuita per GitLab Ultimate.

Per effettuare la trial direttamente sul servizio Saas di GitLab: [GitLab.com trial](https://gitlab.com/-/trial_registrations/new?utm_source=partner&glm_source=partner&glm_content=00161000012W8gKAAS)

Per sperimentare usando un vostro server: [OnPremises Trial](https://page.gitlab.com/sm-free-trial-partner.html?utm_partnerid=00161000012W8gKAAS)
