# frozen_string_literal: true

################################################################################
# Style file for markdownlint.
#
# https://github.com/markdownlint/markdownlint/blob/master/docs/configuration.md
#
# This file is referenced by the project `.mdlrc`.
################################################################################

#===============================================================================
# Start with all built-in rules.
# https://github.com/markdownlint/markdownlint/blob/master/docs/RULES.md
all

# Ignore line length in code blocks.
rule 'MD013', code_blocks: false

# Il default a 3 crea tanti problemi in molti editor
rule 'MD007', indent: 4

# Il default a 0 crea impossibilità di mandare a capo il testo secondo la regola markdown del doppio spazio,
# obbligando l'utilizzo di "invio" e occupando spazio inutile
rule 'MD009', br_spaces: 2

# In the descriptions, you can start with an H2
# rule 'MD002', level: 2

#===============================================================================
# Exclude the rules I disagree with.

# That's a bugged rule
exclude_rule 'MD002'

# I don't care line length
exclude_rule 'MD013'

# Excluding for blog metadata
exclude_rule 'MD041'

exclude_rule 'MD009'

exclude_rule 'MD024'

exclude_rule 'MD030'
